package org.example.service;

public class DistancePredictionServiceTwo {
    public int predict(double consumption, int volume){
        return (int) (volume/consumption * 100);
    }
}

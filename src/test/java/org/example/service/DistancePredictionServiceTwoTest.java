package org.example.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistancePredictionServiceTwoTest {
    @Test
    void shouldPredict(){
        DistancePredictionServiceTwo service = new DistancePredictionServiceTwo();
        double consumption = 7.9;
        int volume = 20;
        int expected = 253;

        int actual = service.predict(consumption, volume);

        assertEquals(expected, actual);
    }


}
